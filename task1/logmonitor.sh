#!/usr/bin/env bash

WORD=$1
LOG=$2

if grep $WORD $LOG &> /dev/null
then
	logger "FOUND $WORD"
else
	exit 1
fi
